# ::docker_build.pp
# ===========================
# Downloads and builds a image according to its Dockerfile.
# Takes a long time, as the servercore image is 10GB and must be verified.
# After that, Docker builds the new image from the Dockerfile and
# the dowloaded base image, which makes the total 20GB.
# ===========================
class win_docker::docker_build {
  if $win_docker::dockerfile_template == 'true' {
    $image      = $win_docker::image
    $docker_dir = $win_docker::docker_dir
    $image_dir  = $win_docker::image_dir

      exec { "${image}_build-dockerfile":
      command   => "docker.exe build -t \"${image}\" \"${image_dir}\"",
      unless    => "if(docker.exe images | sls \"${image}\" ) { exit 0 } else { exit 1 }",
      provider  => powershell,
      returns   => [0,1],
      require   => Exec["${image}_dl-dockerfile"],
    }
  }
}
