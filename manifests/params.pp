# ::params.pp
# ===========================
# Parameter values - see init.pp for information.
# ===========================
class win_docker::params {
  # Edit these bools as needed.
  # If you only want the Docker installation, set both to false.
  $docker_store_pull    = 'true'
  $dockerfile_template  = 'false'

  # Container set to test value as I am unsure of how to make a unique container-id from $image when most images has forwardslashes in their name.
  $container = 'test-container'

  if $docker_store_pull == 'true' {
    # Edit pull_image to set which image to download from Docker Store
    $pull_image  = 'microsoft/nanoserver'
    $image       = $pull_image
    $run_container_after_creation = 'false'
  }

  elsif $dockerfile_template == 'true' {
    # Edit dockerfile_image to set which dockerfile template to download
    $dockerfile_image = 'iis'
    $image       = $dockerfile_image
    $file        = 'Dockerfile'
    $base_url    = 'https://raw.githubusercontent.com/StefanScherer/dockerfiles-windows/master/'
    $url         = "${base_url}${image}/${file}"
    $docker_dir  = "C:/ProgramData/docker"
    $image_dir   = "${docker_dir}/${image}"
    $output      = "${image_dir}/${file}"
    $cmd         = "(New-Object System.Net.WebClient).DownloadFile(\"${url}\", \"${output}\")"
    $run_container_after_creation = 'true'
  }
}
