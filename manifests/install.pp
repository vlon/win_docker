# ::install.pp
# ===========================
# Installs three prerequired Windows resources, and then Docker and its daemon.
# Also sets Docker to the environment path, creates the Docker service,
# and creates the programdata folder.
# Finally it reboots in order to complete the installation.
# Could not use the reboot module as it does not yet support Server 2016.
# Set to install the newest version now. Add -MaximumVersion 17.06.02 in the command for install-docker-pkg if you want to ensure version.
# ===========================
class win_docker::install {
  exec { 'install-nuget':
    command  => 'Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force',
    unless   => 'if(Get-PackageProvider -Name "NuGet") { exit 0 } else { exit 1 }',
    provider => powershell,
  }

  exec { 'install-docker-provider':
    command  => 'Install-Module -Name DockerProvider -Force',
    unless   => 'if(Get-Module -ListAvailable -Name "DockerProvider") { exit 0 } else { exit 1 }',
    provider => powershell,
    require  => Exec['install-nuget'],
  }

  exec { 'install-docker-pkg':
    command  => 'Install-Package -Name docker -ProviderName DockerProvider -Force',
    provider => powershell,
    require  => Exec['install-docker-provider'],
    creates  => 'C:/Program Files/Docker/docker.exe',
    notify   => Exec['restart'],
  }

  exec { 'restart':
    command 	  => 'shutdown -r -f -t 60 -dp "4:1" -c "Machine restart scheduled in 60s to finish Docker installation"',
    path        => 'C:/Windows/System32',
    subscribe   => Exec['install-docker-pkg'],
    refreshonly => true,
  }
}
