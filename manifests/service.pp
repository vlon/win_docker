# ::service.pp
# ===========================
# Ensures that the Docker Windows process is running.
#
# ===========================
class win_docker::service {
  service { 'Docker':
    ensure => running,
    enable => true,
  }
}
