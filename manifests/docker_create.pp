# ::docker_create.pp
# ===========================
# Creates a new container from a image.
# Depending on the use, the container may be run after creation.
# ===========================
class win_docker::docker_create {
  if ($win_docker::dockerfile_template == 'true') or ($win_docker::docker_store_pull == 'true') {
    $image     = $win_docker::image
    $container = $win_docker::container

    if $win_docker::run_container_after_creation == 'true' {

      exec { 'run-container':
        command     => "docker.exe run -d --name \"${container}\" \"${image}\"",
        unless      => "if(docker.exe ps -a | sls \"${container}\" ) { exit 0 } else { exit 1 }",
        provider    => powershell,
        require     => Exec["${image}_build-dockerfile"],
      }
    }
    else {
      exec { 'create-container':
        command   => "docker.exe create -t --name \"${container}\" -i \"${image}\"",
        unless    => "if(docker.exe ps -a | sls \"${container}\" ) { exit 0 } else { exit 1 }",
        provider  => powershell,
        require     => Exec['docker_store_pull'],
      }
    }
  }
}
