# ::uninstall.pp
# ===========================
# Removes docker from Windows.
# 
#============================
class win_docker::uninstall {

  exec { 'stop-containers':
    command  => "docker.exe stop $(docker ps -a -q)",
    provider => powershell,
    before   => Exec['remove-containers'],
  }

  exec { 'remove-containers':
    command => "docker.exe rm $(docker ps -a -q)",
    provider => powershell,
    before  => Exec['remove-images'],
  }

  exec { 'remove-images':
    command => "docker.exe rmi $(docker images -q)",
    provider => powershell,
    before  => Exec['uninstall-docker-pkg'],
  }

  exec { 'uninstall-docker-pkg':
    command  => 'Uninstall-Package -Name docker -ProviderName DockerProvider -Force',
    unless   => 'if(!(Test-Path -Path "C:/Program Files/Docker/docker.exe")) { exit 0 } else { exit 1 }',
    provider => powershell,
    before   => Exec['uninstall-docker-provider'],
  }

  exec { 'uninstall-docker-provider':
    command  => 'Uninstall-Module -Name DockerProvider -Force',
    unless   => 'if(!(Get-Module -ListAvailable -Name "DockerProvider")) { exit 0 } else { exit 1 }',
    provider => powershell,
    before   => Exec['remove-docker-dir'],
  }

  exec { 'remove-docker-dir':
    command  => 'Remove-Item -Recurse -Force "C:/ProgramData/docker"',
    unless   => 'if(!(Test-Path -Path "C:/Program Files/Docker")) { exit 0 } else { exit 1 }',
    provider => powershell,
    require  => Exec['uninstall-docker-provider'],
  }
}
