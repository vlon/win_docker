# ::docker_pull.pp
# ===========================
# Pulls an image from the Docker Store.
# 
# ===========================
class win_docker::docker_pull {
  if $win_docker::docker_store_pull == 'true' {
    $image = $win_docker::image

    exec {'docker_store_pull':
      command   => "docker.exe pull \"${image}\"",
      unless    => "if(docker.exe images | sls \"${image}\" ) { exit 0 } else { exit 1 }",
      provider  => powershell,
      returns   => [0,1],
      require   => Service["Docker"],
      before    => Exec['create-container'],
    }
  }
}
