# ::dockerfile.pp
# ===========================
# Downloads Dockerfiles for common deployments of Windows on Docker,
# using Powershell and a repository of Dockerfile templates.
# ===========================
class win_docker::dockerfile {
  if $win_docker::dockerfile_template == 'true' {
    $base_url    = $win_docker::base_url
    $url         = $win_docker::url
    $file        = $win_docker::file
    $image_dir   = $win_docker::image_dir
    $docker_dir  = $win_docker::docker_dir
    $image       = $win_docker::image
    $container   = $win_docker::container
    $output      = $win_docker::output
    $cmd         = $win_docker::cmd

    file { 'image_dir':
      ensure  => 'directory',
      path    => "C:/ProgramData/docker/${image}",
      require => Service['Docker'],
    }

    exec { "${image}_dl-dockerfile":
      command  => $cmd,
      unless   => "if(Test-Path -Path \"${output}\" ){ exit 0 } else { exit 1 }",
      provider => powershell,
      require  => File['image_dir'],
    }
  }
}
