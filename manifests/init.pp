# win_docker
# ===========================
# win_docker is a Puppet module used to deploy Docker on Windows Server 2016.
#
# Parameters
# ---------
# $image:     Name of the Dockerfile image template from the repo.
# $file:      Name of the file fetched from the repo.
# $base_url:   Part of the Dockerfile repository url.
# $url:       The combined url of base_url, image and file which is used for the file download.
# $docker_dir: The base directory of docker.
# $image_dir:  The directory of the Dockerfile downloaded.
# $output:    The full path of the download location of the Dockerfile.
# $cmd:       The command used to download the Dockerfile.
# $hostname:  The hostname of the container
# $docker_store_pull: bool, whether a image is pulled from the Docker store.
# $dockerfile_template: bool, whether a Dockerfile template is built.
# $run_container_after_creation: bool, whether to run&create or just create the container.
# Dependencies
# ---------
# puppetlabs/puppetlabs-powershell
#
# Creator:
#----------
# Vemund Lonbakken - vemundl@gmail.com
# bitbucket.org/vlon
# - Thanks to the Puppetlabs team for their work on the official docker module,
# really helped me with the setup of this module.
# - Thanks to cyberious/pget for inspiration for the powershell downloading,
# too bad I didn't get pget to work.
# - Shoutouts to Stefan Scherer for his Dockerfile template repo - https://github.com/StefanScherer/dockerfiles-windows
# ===========================
class win_docker(
  $image       = $win_docker::params::image,
  $file        = $win_docker::params::file,
  $base_url    = $win_docker::params::base_url,
  $url         = $win_docker::params::url,
  $docker_dir  = $win_docker::params::docker_dir,
  $image_dir   = $win_docker::params::image_dir,
  $container   = $win_docker::params::container,
  $output      = $win_docker::params::output,
  $cmd         = $win_docker::params::cmd,
  $docker_store_pull   = $win_docker::params::docker_store_pull,
  $dockerfile_template = $win_docker::params::dockerfile_template,
  $run_container_after_creation = $win_docker::params::run_container_after_creation,
) inherits win_docker::params {

  if ($facts['operatingsystemrelease'] != '2016') {

    err('This module is made for Windows Server 2016 only.')
    fail('Unsupported OS / Windows version')

  }  elsif ($facts['operatingsystemrelease'] != '2016') and (facts['osfamily'] == 'windows') {
    warning('It seems like your OS isn´t Windows Server 2016. This module only supports that version - installation will probably fail. Try Docker-for-Windows for Windows 10. ')
  }
}
