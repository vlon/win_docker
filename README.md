# win_docker
IMT3005 Infrastructure as Code - Project

Vemund Lonbakken - # 140682

## Description

README

Win_docker is a Puppet module that can install/uninstall Docker Enterprise Edition on your Windows Server 2016 as well as download, build and run a Docker container. 

If you only want to install Docker and nothing else, remove the value of the image variable. 
If you want to install the container, ensure that you have at least 25 GB free disk space on your C:/ drive.

The module starts off by installing Docker EE. It installs and configures the prerequisites (Set-ExecutionPolicy, Chocolatey and Docker Provider. 
It continues installing the Docker package from the provider, and then restarts the machine to finish the installation. 
This restart is conducted via shutdown.exe, and not by using the reboot module as that does not support WS2016. This means the run is going to fail until the next run after the reboot.
During the next run, the service will be ensured as running and the Dockerfile downloaded. Next, an image will be downloaded and built out of the Dockerfile. 
Be aware that the image download (10GB) and building will take a while (1hr+). This may get your Puppet node out of sync, meaning that it might be one more run interval (default 30 minutes) before you get your report that everything is applied. This might also mean that the run report gets lost between runs. 
Finally, the container will be deployed from the image created, and the desired state is fullfilled.

